import React, {useState, useRef} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  ActivityIndicator,
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  Linking,
} from 'react-native';
import WebView from 'react-native-webview';

const App = () => {
  const [canGoBack, setCanGoBack] = useState(false);
  const [canGoForward, setCanGoForward] = useState(false);
  const [currentUrl, setCurrentUrl] = useState('');
  const [loading, isLoading] = useState(false);
  const webviewRef = useRef(null);
  backButtonHandler = () => {
    if (webviewRef.current) {
      webviewRef.current.goBack();
    }
  };

  frontButtonHandler = () => {
    if (webviewRef.current) {
      webviewRef.current.goForward();
    }
  };
  let jsCode = `
  const meta = document.createElement('meta');
meta.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0');
meta.setAttribute('name', 'viewport');
document.head.appendChild(meta);
     $('a[target="_blank"]').removeAttr('target');
    `;
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.flexContainer}>
        <WebView
          injectedJavaScriptBeforeContentLoaded={jsCode}
          originWhitelist={['*']}
          injectedJavaScript={jsCode}
          javaScriptEnabledAndroid={true}
          scalesPageToFit={false}
          useWebKit={false}
          source={{uri: 'https://wekopacasinoresort.mymapleonline.com/mtest/logout'}}
          startInLoadingState={true}
          renderLoading={() => (
            <ActivityIndicator
              color="black"
              size="large"
              style={styles.spinLoading}
            />
          )}
          ref={webviewRef}
          onShouldStartLoadWithRequest={(navState) => {
            setCanGoBack(navState.canGoBack);
            setCanGoForward(navState.canGoForward);
            console.log(navState.url);
            if (
              navState.url === 'https://www.wekopacasinoresort.com/' ||
              navState.url === 'http://www.wekoparesort.com/'
            ) {
              setCurrentUrl('https://wekopacasinoresort.mymapleonline.com/mtest/logout');
              return false;
            } else {
              setCurrentUrl(navState.url);
              return true;
            }
          }}
          onNavigationStateChange={(navState) => {
            setCanGoBack(navState.canGoBack);
            setCanGoForward(navState.canGoForward);
            if (navState.url === 'https://www.wekopacasinoresort.com/') {
              setCurrentUrl('https://wekopacasinoresort.mymapleonline.com/mtest/logout');
              return false;
            } else {
              setCurrentUrl(navState.url);
              return true;
            }
          }}
        />
        <View style={styles.tabBarContainer}>
          <TouchableOpacity onPress={backButtonHandler}>
            <Text style={styles.button}>Back</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={frontButtonHandler}>
            <Text style={styles.button}>Forward</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
  },
  spinLoading: {
    position: 'absolute',
    top: Dimensions.get('window').height / 2,
    left: Dimensions.get('window').width / 2,
    flex: 1,
  },
  tabBarContainer: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'black',
  },
  button: {
    color: 'white',
    fontSize: 24,
  },
});

export default App;
